"""Den Aufbau dieser Klassen habe ich vom Udemy Tutorial übernommen.
Die Koordinaten habe ich selbst gewählt"""
from parameters import Parameters
from tetrispiece import TetrisPiece

"""In jeder dieser Klassen, wird ein bestimmte Form mit mehreren Blöcken gebildet.
   Coords stehen hier für die Koordinaten, mit welchen ein Spielstein erschaffen wird.
"""


class Z(TetrisPiece):
    def __init__(self, center):
        """Konstruktor der Klasse Z"""
        x, y = center
        coords = [(x-1, y), (x, y), (x, y-1), (x+1, y-1)] #Koordinaten, welche den Spielstein 
        #bilden und ihm seine Form geben.
        super().__init__(coords)
        self.center_block = self.block_list[1] #Der Stein in der Mitte (center block) wird definiert, 
        #damit das Programm weiss, um welchen Stein, die Spielfigurt rotiert werden soll.
        self.rotation_change = True


class S(TetrisPiece):
    def __init__(self, center):
        x, y = center
        coords = [(x, y), (x+1, y), (x, y-1), (x-1, y-1)]
        super().__init__(coords)
        self.center_block = self.block_list[0]
        self.rotation_change = True


class I(TetrisPiece):
    def __init__(self, center):
        x, y = center
        coords = [(x-1, y), (x, y), (x+1, y), (x+2, y)]
        super().__init__(coords)
        self.center_block = self.block_list[1]
        self.rotation_change = True
        self.rotation = -1


class J(TetrisPiece):
    def __init__(self, center):
        x, y = center
        coords = [(x-1, y), (x, y), (x+1, y), (x+1, y-1)]
        super().__init__(coords)
        self.center_block = self.block_list[1]


class L(TetrisPiece):
    def __init__(self, center):
        x, y = center
        coords = [(x-1, y), (x, y), (x+1, y), (x-1, y-1)]
        super().__init__(coords)
        self.center_block = self.block_list[1]


class T(TetrisPiece):
    def __init__(self, center):
        x, y = center
        coords = [(x-1, y), (x, y), (x+1, y), (x, y-1)]
        super().__init__(coords)
        self.center_block = self.block_list[1]


class X(TetrisPiece):
    def __init__(self, center):
        x, y = center
        coords = [(x-1, y), (x, y), (x+1, y), (x, y-1), (x, y+1)]
        super().__init__(coords)
        self.center_block = self.block_list[1]


class K(TetrisPiece):
    def __init__(self, center):
        x, y = center
        coords = [(x+1, y), (x, y), (x, y+1)]
        super().__init__(coords)
        self.center_block = self.block_list[1]


class M(TetrisPiece):
    def __init__(self, center):
        x, y = center
        coords = [(x-1, y), (x, y), (x, y+1)]
        super().__init__(coords)
        self.center_block = self.block_list[1]


class O(TetrisPiece):
    def __init__(self, center):
        x, y = center
        coords = [(x, y), (x+1, y), (x, y-1), (x+1, y-1)]
        super().__init__(coords)
        self.center_block = self.block_list[0]

    def rotate(self, board):
        return


Shapes = [I, J, L, O, S, T, Z, X, K, M]
