"""Diese Funktion habe ich selbst geschrieben"""
import arcade
from parameters import Parameters
from tetrisapp import TetrisApp


def main():
    #Diese Datei muss ausgeführt werden, um das Spiel zu starten. 
    #In all den anderen Dateien wurde das Spiel geschrieben und gezeichnet,
    #doch hier werden alle wichtigen Funktionen und Klassen aufgerufen und gestartet mit arcade.run().
    TetrisApp(Parameters.Width, Parameters.Height, 'Tetris')
    arcade.run()


main()
