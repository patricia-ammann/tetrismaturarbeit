"""
    Die Klasse Block habe ich hauptsächlich nach dem Udemy Tutorial gestaltet.
    Den Teil zum Zeichnen der Hilfslinien habe ich weggelassen und die Klasse auf
    das Notwendige reduziert.
"""
import arcade
import random
from gridcoords import Coords
from parameters import Parameters


class Block(arcade.Sprite):
    """
    Die Block-Klasse repräsentiert einen einzelnen Block, das kleinste Element, das auf dem Board Platz findet.
    Ein Tetris-Piece besteht in der Regel aus mehreren Blöcken, die in der gewünschten Form zusammenhängen.

    Die Block-Klasse erbt von arcade.Sprite, weil die Instanzen auch tatsächlich sichtbare Einheiten sind.
    """

    def __init__(self, filename, grid_pos):
        """Konstruktor der Klasse Block"""

        self.grid_x, self.grid_y = grid_pos #Diese Variablen werden bereits vor dem super().__init__ inizialisiert, 
        #da diese im super().__init__ gebraucht werden und Python die Befehle immer von oben nacht unten ausführt, 
        #würde hier sonst eine Fehlermeldung angezeigt werden.
        x, y = Coords.grid_to_coords(self.grid_x, self.grid_y)

        super().__init__(filename, center_x=x, center_y=y, scale=Parameters.Block_Size)

    def move(self, dx, dy):
        #Verändert die Position des Blocks relativ um die Werte dx und dy.
        self.grid_x += dx
        self.grid_y += dy
        self.center_x, self.center_y = Coords.grid_to_coords(
            self.grid_x, self.grid_y)

    def can_move(self, board, dx, dy):
        #Prüft ob der Spielstein bewegt werden kann und gibt dann entweder den Wert 
        #True oder False der Funktion move zurück.
        return board.is_valid_position(self.grid_x + dx, self.grid_y + dy)
