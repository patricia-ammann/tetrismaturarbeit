"""
    In dieser Datei habe ich die Funktion do_hard_drop vom Udemy Tutorial
    übernommen und sonst alles selbst geschrieben und initialsisert. 
    Ich habe zusätzlich noch die Klasse AppState (eine Aufzählung/Enumeration),
    die Variable state und die Methode kill_block selbst geschrieben und an 
    diversen Stellen zur Ablaufsteuerung genutzt.
"""
import arcade
import random
from enum import Enum
from parameters import Parameters
from board import Board
from tetrispiecefactory import TetrisPieceFactory
from tetrispiece import TetrisPiece
from tetrisshapes import Shapes


class AppState(Enum):
    READY = 1
    PLAYING = 2
    PAUSED = 3
    LOST = 4
    WON = 5
    JOKER = 6
    REMOVEROW = 7


class TetrisApp(arcade.Window):
    """
        In dieser Klasse wird das Fenster aufgerufen, sobald das Spiel gestartet wurde und 
        sehr viele Sprites(Spielfiguren) wurden in dieser Klasse difiniert.
        Dies ist die Hauptklasse  in der schlussendlich alle anderen Klassen zusammenkommen.

        Diese Klasse erbt von den Klasse arcade.Window, welche der erbenden Klasse 
        gewissen Parameter und Funktionen mitgibt, welche bereits definiert wurden und 
        jetzt einfach nur noch gebraucht werden müssen.
    """

    def __init__(self, width, height, title):
        """Konstruktor der Klasse TetrisApp"""
        super().__init__(width, height, title)
        arcade.set_background_color(arcade.color.CAMEO_PINK)
        self.setup()  # Das unten definierte Setup wird aufgerufen und das Fenster wird mit allem, was das
        # Setup beinhaltet, gezeichnet.

    def setup(self):
        """setup bereitet das Spiel für eine neue Spielrunde vor und setzt alle nötigen Variablen und Zustände"""
        self.state = AppState.READY

        # --- Diverse Bildpfade und Positionen --------------------------------
        # Das Logo für Tetris:
        self.logo = arcade.Sprite(
            './images/Logo.png', 0.08, center_x=Parameters.Width/2-10, center_y=Parameters.Height-60)

        # Wenn der Spieler verloren hat:
        self.game_over_picture = arcade.Sprite(
            './images/Game_Over.png', 0.8, center_x=Parameters.Width/2, center_y=Parameters.Height/2)

        # Wenn der Spieler gewonnen hat:
        self.won_picture = arcade.Sprite(
            './images/You_Win.png', 1.2, center_x=Parameters.Width/2, center_y=Parameters.Height/2)

        # Das Spielgitter, in dem sich die Spielfiguren bewegen:
        self.board = Board(Parameters.Grid_Width, Parameters.Grid_Height)

        # Der erste Spielstein, der kreiert wird:
        self.current_piece = TetrisPieceFactory.createPiece()

        self.delay = 30
        self.delay2 = 60
        self.frames = 0
        self.frames2 = 0
        self.joker = 3
        self.jokerFlash = -1
        self.second = 0
        self.minute = 0

    def drawBackground(self):
        """Die Zeichnungen für den Hintergrund (die beiden Farben)"""
        arcade.draw_rectangle_filled(
            356.25, 742.5, 712.5, 225, arcade.color.TURKISH_ROSE)
        arcade.draw_rectangle_outline(
            270, 315, 535, 625, arcade.color.BLACK, border_width=5)

    def drawHUD(self):
        """Die Anweisungen ganz am Anfang des Spieles"""
        arcade.draw_text('Wilkommen zu Tetris. \n Du bekommst Punkte für alle Reihen, die du füllen kannst, \n aber du verlierst wenn du ganz oben ankommst. \n Wenn du 10 Reihen füllen kannst, gewinnst du das Spiel. \n Bitte drücke s um das Spiel zu starten!',
                         Parameters.Width/2, Parameters.Height/2-100, arcade.color.BYZANTIUM, 15, align="center", anchor_x="center")
        arcade.draw_text('Du kannst die Spielsteine mit den Pfeiltasten bewegen. \n S = Start \n Pfeiltaste oben = Rotieren \n P = Pause \n C = Fortsetzen \n Leertaste = Spielstein springt direkt nach unten',
                         Parameters.Width/2, Parameters.Height/2-250, arcade.color.BYZANTIUM, 15, align="center", anchor_x="center")

    def drawHUDPlaying(self):
        """Die Beschriftungen, welche die Anzahl gefüllter Reihen, verfügbarer Joker und die Spielzeit anzeigen"""
        arcade.draw_text(f'Gefüllte Reihen: {self.board.deleted_rows}', Parameters.Width /
                         2-170, 680, arcade.color.BLACK, 15, align='center', anchor_x='center')
        arcade.draw_text(f'Verfügbare Joker: {self.joker}', Parameters.Width/2+170,
                         680, arcade.color.BLACK, 15, align='center', anchor_x='center')
        arcade.draw_text(f'Zeit: {self.minute} min und {self.second} s', Parameters.Width/2,
                         635, arcade.color.BLACK, 20, align='center', anchor_x='center')

    def drawKilling(self):
        """Hinweis wenn ein Joker eingesetzt wird um den aktellen Block zu zerstören"""
        arcade.draw_text(f'Killing Block...', Parameters.Width /
                         2, Parameters.Height / 2, arcade.color.BLACK, 30, align='center', anchor_x='center')

    def drawGameOver(self):
        """Die Anweisung, wenn man verloren hat"""
        self.game_over_picture.draw()
        arcade.draw_text('Bitte drücke r um neu zu beginnen', Parameters.Width/2, Parameters.Height /
                         2-200, arcade.color.BLACK, 26, align='center', anchor_x='center')

    def drawWon(self):
        """Die Anweisung, wenn man gewonnen hat"""
        self.won_picture.draw()
        arcade.draw_text('Gratulationen! \n Du hast gewonnen. \n Bitte drücke r um neu zu beginnen',
                         Parameters.Width/2, Parameters.Height/2-300, arcade.color.BLACK, 26, align='center', anchor_x='center')

    def drawPaused(self):
        """Anweisung, wenn man Pause gedrückt hat"""
        arcade.draw_text('Bitte drücke c um fortzusetzen!', Parameters.Width/2, Parameters.Height/2,
                         arcade.color.BLACK, 26, align='center', anchor_x='center')

    def on_draw(self):
        """Hier werden alle zu zeichnenden Objekte abhängig vom Application State gezeichnet"""
        arcade.start_render()

        if self.state == AppState.READY:
            self.drawHUD()

        if self.state == AppState.PLAYING:
            self.current_piece.draw()
            self.board.draw()
        
        if self.state == AppState.REMOVEROW:
            self.current_piece.draw()
            self.board.draw()
            self.board.hightlight_row()

        if self.state == AppState.LOST:
            self.board.draw()
            self.drawGameOver()

        if self.state == AppState.WON:
            self.board.draw()
            self.drawWon()

        if self.state == AppState.PAUSED:
            self.current_piece.draw()
            self.board.draw()
            self.drawPaused()

        if self.state == AppState.JOKER:
            self.board.draw()
            self.drawKilling()
            if self.jokerFlash == -1:
                self.jokerFlash = 0
            else:
                self.jokerFlash += 1
            JokerIndex = int(self.jokerFlash / 10)
            if JokerIndex < len(Parameters.JokerColors):
                arcade.set_background_color(Parameters.JokerColors[JokerIndex])
            else:
                arcade.set_background_color(arcade.color.CAMEO_PINK)
                self.jokerFlash = -1
                self.state = AppState.PLAYING

        self.drawBackground()
        self.drawHUDPlaying()
        self.logo.draw()

    def on_update(self, delta_time):
        """Updatet das Spiel 60 mal in der Sekunde"""

        if self.state == AppState.PLAYING:
            self.frames += 1
            if self.frames % self.delay == 0:
                self.do_move('Down')

            if self.board.should_hightlight_complete_row():
                self.highlight_count_down = 50
                self.state = AppState.REMOVEROW

        if self.state == AppState.REMOVEROW:
            self.highlight_count_down -= 1
            if self.highlight_count_down <= 0:
                self.highlight_count_down = 0
                self.board.remove_complete_rows()
                if self.board.should_hightlight_complete_row():
                    self.highlight_count_down = 50
                    # verbleibe in AppState.REMOVEROW
                else:
                    self.state = AppState.PLAYING

        if self.board.deleted_rows == 10:
            self.state = AppState.WON

        if self.state == AppState.PLAYING:
            self.frames2 += 1
            if self.frames2 % self.delay2 == 0:
                self.second += 1
            if self.second == 60:
                self.minute += 1
                self.second = 0

    def kill_block(self):
        """Lässt eine Spielfigur verschwinden, wenn der Spieler ein Joker eingesetzt hat"""
        if self.joker == 0:
            return
        del self.current_piece
        self.current_piece = TetrisPieceFactory.createPiece()
        self.joker -= 1
        self.state = AppState.JOKER

    def do_rotate(self):
        """Rotiert den Spielstein"""
        self.current_piece.rotate(self.board)

    def do_move(self, direction):
        """Bewegt den Spielstein in die angegebene Richtung"""
        if self.state == AppState.PLAYING:
            dx, dy = Parameters.Direction[direction]
            if self.current_piece.can_move(self.board, dx, dy):
                self.current_piece.move(dx, dy)
            elif direction == 'Down':
                self.board.add_piece_to_board(self.current_piece)
                self.current_piece = TetrisPieceFactory.createPiece()
                if not self.current_piece.can_move(self.board, 0, 0):
                    self.state = AppState.LOST

    def do_hard_drop(self):
        """Lässt den Spielstein nach unten schnellen"""

        while self.current_piece.can_move(self.board, 0, -1):
            self.current_piece.move(0, -1)
        self.board.add_piece_to_board(self.current_piece)
        # self.board.remove_complete_rows()
        self.current_piece = TetrisPieceFactory.createPiece()

    def on_key_press(self, key, modifiers):
        """
            Hier sind alle Tasten, welche im Spiel gebraucht werden, einer Funktion zugewiesen.
            Es gibt Tasten, welche man nur bedienen kann, wenn gewissen Bedingungen nicht erfüllt sind.
            Das ist mit dem App-State definiert.
        """

        if self.state == AppState.READY:
            # im READY state kann das Spiel gestartet werden, andere Aktionen sind noch nicht zulässig
            if key == arcade.key.S:
                self.state = AppState.PLAYING

        if self.state == AppState.PLAYING:
            # die meisten Aktionen sind zulässig, wenn das Spiel läuft
            if key == arcade.key.DOWN:
                self.do_move('Down')
            if key == arcade.key.RIGHT:
                self.do_move('Right')
            if key == arcade.key.LEFT:
                self.do_move('Left')
            if key == arcade.key.UP:
                self.do_rotate()
            if key == arcade.key.P:
                self.state = AppState.PAUSED
            if key == arcade.key.SPACE:
                self.do_hard_drop()
            if key == arcade.key.K:
                self.kill_block()

        if self.state == AppState.REMOVEROW:
            # keine Aktionen möglich während dem Entfernen einer vollen Zeile
            pass

        if self.state == AppState.JOKER:
            # keine Aktionen möglich während dem Entfernen des aktuellen Blocks
            pass

        if self.state == AppState.PAUSED:
            # wenn das Spiel pausiert ist, dann kann nur die Wiederaufnahme des Spieles ausgelöst werden
            if key == arcade.key.C:
                self.state = AppState.PLAYING

        if self.state == AppState.WON or self.state == AppState.LOST:
            # wenn das Spiel vorbei ist, dann kann es nur neu gestartet werden
            if key == arcade.key.R:
                self.setup()
                self.state = AppState.PLAYING
