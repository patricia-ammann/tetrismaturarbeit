"""Alles bis auf die Funktione draw und den Konstruktor habe ich vom
Udemy Tutorial übernommen."""
import arcade
import random
from gridcoords import Coords
from parameters import Parameters


class Board():
    """
    Die Board-Klasse repräsentiert einen einzelnen Block, das kleinste Element, das auf dem Board Platz findet.
    Ein Tetris-Piece besteht in der Regel aus mehreren Blöcken, die in der gewünschten Form zusammenhängen.

    Die Block-Klasse erbt von keiner anderen Klasse, sie ist ein Teil der Spiellogik.

    Die unterste Zeile auf dem Spielbrett (Board) hat den Index 0.
    Die oberste Zeile auf dem Spielbrett (Board) hat den Index self.height - 1
    """

    def __init__(self, width, height):
        """Konstruktor der Klasse Board"""
        # width und height sind in Spielkoordinaten, nicht Bildschirmpixel.
        self.width = width
        self.height = height
        self.blocks_on_board = {}
        self.block_list = arcade.SpriteList()
        self.deleted_rows = 0

    def draw(self):
        # Der Block wird gezeichnet:
        self.block_list.draw()

    def is_valid_position(self, x, y):
        # Hier überprüft das Programm, welche Felder im Board schon besetzt sind und welche noch frei sind.
        # Wenn der Block nicht zu links, zu rechts oder unterhalb vom Spielrand ist und das Feld nicht
        # bereits von einem anderen Stein besetzt ist, wird der Wert True zurückgegebn.
        if x >= self.width or x < 0:
            return False
        elif y < 0:
            return False
        elif (x, y) in self.blocks_on_board:
            return False
        return True

    def add_piece_to_board(self, piece):
        # Hier wird die Spielfigur dem Spielfeld hinzugefügt.
        for block in piece.block_list:
            self.block_list.append(block)
            self.blocks_on_board[(block.grid_x, block.grid_y)] = block

    def should_hightlight_complete_row(self):
        """Highlight der nächsten vollständigen Zeile"""
        hasCompleteRow = False
        for row in range(self.height - 1, -1, -1):
            if self.is_row_complete(row):
                hasCompleteRow = True
                # Berechne X, Y und Breite und Höhe des Highlight-Bereichs
                # Der Rahmen soll eine zufällige Farbe aus einer vorgegebenen Liste haben
                self.hl_x = Parameters.Grid_Width * Parameters.TBlockSize / 2
                self.hl_y = row * Parameters.TBlockSize + Parameters.TBlockSize / 2
                self.hl_width = Parameters.TBlockSize * Parameters.Grid_Width - 14
                self.hl_height = Parameters.TBlockSize - 6
                self.hl_color = random.choice(Parameters.RowHighlightColors)
        return hasCompleteRow

    def hightlight_row(self):
        """zeichnet einen Rahmen um eine Zeile"""
        arcade.draw_rectangle_outline(
            self.hl_x, self.hl_y, self.hl_width, self.hl_height, self.hl_color, border_width=5)

    def remove_complete_rows(self):
        """ 
        Entfernt vollständige Reihen vom Spielbrett (Board).
        Siehe auch Dokumentation zur Maturarbeit.
        """
        # row (ganzzahliger Wert) zählt von (self.height-1) runter bis zu 0
        # https://www.w3schools.com/python/ref_func_range.asp
        for row in range(self.height - 1, -1, -1):
            if self.is_row_complete(row):
                self.delete_row(row)
                self.move_down_rows(row + 1)
                break

    def is_row_complete(self, y):
        """ Diese Funktion überprüft, ob in einer Reihen jedes Feld von 
        einer Spielfigur besetzt ist. Wenn nicht gibt sie den WErt False zurück, 
        ansonsten den Wert True.
        """
        for x in range(self.width):
            if (x, y) not in self.blocks_on_board:
                return False
        return True

    def delete_row(self, y):
        """Löscht jeden Block, sobald die funktion is_row_complete den WErt True zurückgeben hat."""
        for x in range(self.width):
            block = self.blocks_on_board[(x, y)]
            block.remove_from_sprite_lists()
            self.blocks_on_board.pop((x, y))
        self.deleted_rows += 1

    def move_down_rows(self, y_start):
        """Die vollständige Reihe wird gelöscht und die Reihen oberhalb werden jeweils 
        um 1(Y-Wert) Feld nach unten gerückt.
        """
        for row in range(y_start, self.height):
            for col in range(self.width):
                if (col, row) in self.blocks_on_board:
                    block = self.blocks_on_board[(col, row)]
                    self.blocks_on_board.pop((col, row))
                    block.move(0, -1)
                    self.blocks_on_board[(col, row-1)] = block
