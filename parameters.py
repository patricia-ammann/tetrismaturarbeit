"""Block_Size, Start_Pos und Direction sind Variablen, welche ich von dem Udemy 
Tutorial übernommen habe. Der Rest wurde von mir selbst inizialisiert"""
import random
import arcade


class Parameters:
    """
    Klasse die alle Parameter als statische Variablen beinhaltet.
    Im Udemy Tutorial waren die Variablen als globale Variablen definiert.
    Ich habe sie jedoch alle zusammen in eine Klasse zusammen getan um den Code übersichtlich zu behalten.
    """

    TestMode = False

    TBlockSize = 45

    Grid_Width = 12
    Width = TBlockSize * Grid_Width
    Grid_Height = 19
    Height = TBlockSize * Grid_Height

    Block_Size = 0.09

    Direction = {'Left': (-1, 0), 'Right': (1, 0), 'Down': (0, -1)}

    # https://arcade.academy/arcade.color.html
    JokerColors = [arcade.color.AIR_SUPERIORITY_BLUE, arcade.color.APPLE_GREEN,
                   arcade.color.CAMEO_PINK, arcade.color.BANANA_YELLOW]

    RowHighlightColors = [
        arcade.color.BLUE, arcade.color.GREEN, arcade.color.RED, arcade.color.YELLOW]
