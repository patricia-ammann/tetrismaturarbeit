"""
    Diese Klasse habe ich selbst erstellt, um das Spiel besser testen zu können.
"""
import random
from parameters import Parameters
from tetrisshapes import Shapes, O


class TetrisPieceFactory:
    @staticmethod
    def createPiece():
        start_pos = (random.randint(2, 9), 15)
        if Parameters.TestMode:
            return O(start_pos)
        else:
            return random.choice(Shapes)(start_pos)
