"""
    Die Klasse habe ich selbst initialisiert um globale Funktionen zu vermeiden.
    Die Funktion grid_to_coords selbst habe ich vom Udemy Tutorial übernommen.
"""
import arcade
from parameters import Parameters


class Coords:
    """
    Diese Klasse wandelt die Spielbrett-Werte in Koordinaten (Bildschirmpixel) um.
    Hier wird eine statische Methode gebraucht, um den Parameter self zu umgehen 
    und so ist die Funktion grid_to_coords Klasse zugewiesen und nicht einer Instanz
    der Klasse, also kein Objekt von ihr
    """
    @staticmethod
    def grid_to_coords(grid_x, grid_y):
        center_x = Parameters.TBlockSize/2 + grid_x * Parameters.TBlockSize
        center_y = Parameters.TBlockSize/2 + grid_y * Parameters.TBlockSize
        return center_x, center_y
