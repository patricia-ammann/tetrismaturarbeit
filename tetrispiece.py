"""Die Funktionen move, can_move, rotate und can_rotate, sowie die Variablen
self.rotation und seld.rotation_change habe ich aus dem 
Udemy Tutorial übernommen, der Rest wurde von mir selbst geschrieben."""
import arcade
import random
from block import Block
from gridcoords import Coords


class TetrisPiece():
    """In dieser Klasse sind alle Rahmenbedingungen und Eigenschafte eines Spielsteines(=Tetrispiece) definiert.

    Sie erbt von keiner anderen Klasse.
    
    Hier werden den Spielsteinen ihre Farben, ihre Fähigkeiten sich zu bewegen und zu rotieren zugewiesen."""

    color_list = ['./images/blue.jpeg', './images/green.jpeg', './images/orange.jpeg', './images/red.jpeg',
                  './images/violet.jpeg', './images/yellow.jpeg']

    def __init__(self, coords):
        """Konstruktor der Klasse TetrisPiece"""
        self.block_list = arcade.SpriteList()
        self.center_block = None

        filename = random.choice(TetrisPiece.color_list) #Die Farben der Spielsteinen sind eigentlich Bilder 
        #und werden im Prgramm mit Filename bezeichnet.
        for grid_pos in coords:
            block = Block(filename, grid_pos)
            self.block_list.append(block)

        self.rotation = 1
        self.rotation_change = False

    def draw(self):
        #Zeichnet die Blöcke.
        self.block_list.draw()

    def move(self, dx, dy):
        #Mit dieser Funktion ist der Spieler in der Lage den Tetrispiece zu bewegen.
        for block in self.block_list:
            block.move(dx, dy)

    def can_move(self, board, dx, dy):
        #Hier überprügt das Prgramm, ob der Spielstein überhaupt bewegbar ist.
        for block in self.block_list:
            if not block.can_move(board, dx, dy):
                return False

        return True

    def can_rotate(self, board):
        #Überprüft, ob der Spielstein rotierbar ist.

        for block in self.block_list:
            x = self.center_block.grid_x - self.rotation * \
                self.center_block.grid_y + self.rotation * block.grid_y
            y = self.center_block.grid_y + self.rotation * \
                self.center_block.grid_x - self.rotation * block.grid_x
            if not board.is_valid_position(x, y):
                return False
        return True

    def rotate(self, board):
        #Wenn die Funktion can_rotate den Wert True zurückgibt, 
        #dann gibt diese Funktion den Befehl den Spielstein zu rotieren.
        if self.can_rotate(board):
            for block in self.block_list:
                x = self.center_block.grid_x - self.rotation * \
                    self.center_block.grid_y + self.rotation * block.grid_y
                y = self.center_block.grid_y + self.rotation * \
                    self.center_block.grid_x - self.rotation * block.grid_x
                block.grid_x = x
                block.grid_y = y
                block.center_x, block.center_y = Coords.grid_to_coords(x, y)
            if self.rotation_change:
                self.rotation *= -1
